import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DefaultComponent } from './components/layouts/default/default.component';
import { DashboardComponent } from './components/modules/dashboard/dashboard.component';
import { AssetMasterComponent } from './components/modules/asset-master/asset-master.component';
import { EmployeeMasterComponent } from './components/modules/employee-master/employee-master.component';


const routes: Routes = [
  {
    path: '',
    component: DefaultComponent,
    children: [
      {
        path: '',
        component: DashboardComponent
      },
      {
        path: 'asset-master',
        component: AssetMasterComponent
      },
      {
        path: 'empolyee-master',
        component: EmployeeMasterComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
